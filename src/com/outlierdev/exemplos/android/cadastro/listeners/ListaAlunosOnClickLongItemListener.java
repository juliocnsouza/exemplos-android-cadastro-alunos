package com.outlierdev.exemplos.android.cadastro.listeners;

import com.outlierdev.exemplos.android.cadastro.ListaAlunosActitvity;
import com.outlierdev.exemplos.android.cadastro.model.entidade.Aluno;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;

/**
 * Classe que implementa o listener para a lista de Alunos clique long
 * @see ListaAlunosActitvity
 * @author juliocnsouza
 *
 */
public class ListaAlunosOnClickLongItemListener implements
		OnItemLongClickListener {

	private Context context;
	private Aluno alunoSelecionado;
	
	public ListaAlunosOnClickLongItemListener(Context context) {
		super();
		this.context = context;
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View view, int posicao,
			long id) {
		setAlunoSelecionado((Aluno) adapter.getItemAtPosition(posicao));
		Toast.makeText(context, "Você clicou em : " + adapter.getItemAtPosition(posicao), Toast.LENGTH_SHORT).show();;
		return false;
	}

	public Aluno getAlunoSelecionado() {
		return alunoSelecionado;
	}

	public void setAlunoSelecionado(Aluno alunoSelecionado) {
		this.alunoSelecionado = alunoSelecionado;
	}

}
