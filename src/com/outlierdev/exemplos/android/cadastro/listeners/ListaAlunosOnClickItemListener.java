package com.outlierdev.exemplos.android.cadastro.listeners;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.outlierdev.exemplos.android.cadastro.ListaAlunosActitvity;
import com.outlierdev.exemplos.android.cadastro.R;
import com.outlierdev.exemplos.android.cadastro.model.entidade.Aluno;

/**
 * Classe que implementa o listener para a lista de Alunos clique curto
 * @see ListaAlunosActitvity
 * @author juliocnsouza
 *
 */
public class ListaAlunosOnClickItemListener implements OnItemClickListener {

	private Context context;
	private ListaAlunosActitvity actitvity;
	private Aluno alunoSelecionado;
	
	public ListaAlunosOnClickItemListener(Context context, ListaAlunosActitvity actitvity) {
		super();
		this.setContext(context);
		this.actitvity = actitvity;
	}
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int posicao, long id) {
		alunoSelecionado = (Aluno) adapter.getItemAtPosition(posicao);
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setIcon(R.drawable.todos_alunos);
		dialog.setTitle("DADOS DO ALUNO");
		dialog.setMessage(getDadoAluno(alunoSelecionado));
		OnClickListener editarAlunoListener = new EditarAlunoOnClickListener(context, actitvity, alunoSelecionado);
		dialog.setPositiveButton("Editar", editarAlunoListener );
		OnClickListener voltarClickListener = new VoltarOnClickListener();
		dialog.setNegativeButton("Voltar", voltarClickListener );
		dialog.show();
		
	}
	
	private String getDadoAluno(Aluno alunoSelecionado){
		StringBuilder sb = new StringBuilder();
		sb.append("NOME: " + alunoSelecionado.getNome().toUpperCase() + "\n");
		sb.append("TELEFONE: " + alunoSelecionado.getTelefone().toUpperCase() + "\n");
		sb.append("ENDEREÇO: " + alunoSelecionado.getEndereco().toUpperCase() + "\n");
		sb.append("SITE: " + alunoSelecionado.getSite().toLowerCase() + "\n");
		sb.append("IMG: " + alunoSelecionado.getLocalImagem() + "\n");
		sb.append("NOTA: " + alunoSelecionado.getNota() );
		return sb.toString();
	}
	
	public Aluno getAlunoSelecionado() {
		return alunoSelecionado;
	}
	public Context getContext() {
		return context;
	}
	public void setContext(Context context) {
		this.context = context;
	}

}
