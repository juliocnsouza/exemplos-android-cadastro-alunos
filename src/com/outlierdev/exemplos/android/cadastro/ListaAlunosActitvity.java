package com.outlierdev.exemplos.android.cadastro;

import java.sql.SQLException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.outlierdev.exemplos.android.cadastro.adapter.ListaAlunosAdapter;
import com.outlierdev.exemplos.android.cadastro.dao.GenericDAO;
import com.outlierdev.exemplos.android.cadastro.listeners.DeletarContextMenuItemClickListener;
import com.outlierdev.exemplos.android.cadastro.listeners.LigarContextMenuItemClickListener;
import com.outlierdev.exemplos.android.cadastro.listeners.ListaAlunosOnClickItemListener;
import com.outlierdev.exemplos.android.cadastro.listeners.ListaAlunosOnClickLongItemListener;
import com.outlierdev.exemplos.android.cadastro.listeners.enviarEmailContextMenuItemClickListener;
import com.outlierdev.exemplos.android.cadastro.model.entidade.Aluno;

/**
 * Activity da Lista de Alunos
 * 
 * @author juliocnsouza
 *
 */
public class ListaAlunosActitvity extends Activity {

    private Context context = ListaAlunosActitvity.this;
	private List<Aluno> alunos;
	private int resource;
	private ListaAlunosAdapter adapter;
	private ListView listaAlunos;
	private ListaAlunosOnClickItemListener listenerCliqueCurto;
	private ListaAlunosOnClickLongItemListener listenerCliqueLongo;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_alunos);
        
        resource = R.layout.linha_listaalunos;
        listaAlunos = (ListView) findViewById(R.id.lv_alunos);
        initAlunos();
        registerForContextMenu(listaAlunos);
        
		listenerCliqueCurto = new ListaAlunosOnClickItemListener(context, this);
		listaAlunos.setOnItemClickListener(listenerCliqueCurto);
		
		listenerCliqueLongo = new ListaAlunosOnClickLongItemListener(context);
		listaAlunos.setOnItemLongClickListener(listenerCliqueLongo );
    }

	/**
	 * metodo que inicializa / atualiza dados a lista de alunos
	 */
	private void initAlunos() {
		try {
			alunos = new GenericDAO().getLista(this, Aluno.class);
		} catch (SQLException e) {
			Toast.makeText(context, "Ocorreu um erro: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		adapter = new ListaAlunosAdapter(this, alunos);
		listaAlunos.setAdapter(adapter);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initAlunos();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lista_alunos, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemSelecionado = item.getItemId();
		switch (itemSelecionado) {
		case R.id.novo_aluno:
			Intent irParaCadastroAluno = new Intent(context, FormularioAlunoActivity.class);
			startActivity(irParaCadastroAluno);
			return false;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		Aluno alunoSelecionado = listenerCliqueLongo.getAlunoSelecionado();
		MenuItem ligar = menu.add("Ligar");
		ligar.setOnMenuItemClickListener(new LigarContextMenuItemClickListener(alunoSelecionado));
		
		menu.add("Enviar SMS");
		menu.add("Achar no Mapa");
		menu.add("Navegar no site");
		
		MenuItem email = menu.add("Enviar E-mail");
		email.setOnMenuItemClickListener(new enviarEmailContextMenuItemClickListener(alunoSelecionado));
		
		
		MenuItem deletar = menu.add("Deletar");
		deletar.setOnMenuItemClickListener(new DeletarContextMenuItemClickListener(context, this, alunoSelecionado));
	}
	
	/**
	 * metodo usado para atualizar a lista restartando a activity
	 * importante verificar se existem parametros (Bundle) a serem recuperados.
	 * usada para atualizar lista após execução de ações no ContextMenu
	 * @see DeletarContextMenuItemClickListener para exemplo
	 */
	public void refresh(){
		Intent refresh = new Intent(context, ListaAlunosActitvity.class);
		startActivity(refresh);
		finish();
	}
	
	
	
	
	
	
	
	
	
	
}
